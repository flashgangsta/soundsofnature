package com.son {
	import com.flashgangsta.managers.MappingManager;
	import com.flashgangsta.utils.StringUtil;
	import feathers.controls.ScrollContainer;
	import flash.display.DisplayObject;
	import flash.utils.ByteArray;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.filters.BlurFilter;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	public class PreviewsContainer extends ScrollContainer {
		
		static public const DISPLAYED_ITEMS_NUM:int = 4;
		
		static private const SPACING_H:int = 40;
		static private const SPACING_V:int = 25;
		
		private var pagesList:Vector.<Sprite> = new Vector.<Sprite>();
		private var helper:Helper = Helper.getInstance();
		private var selectedPreview:Preview;
		
		public function PreviewsContainer() {
			super();
		}
		
		public function init(locationModelsList:Vector.<LocationModel>):void {
			var preview:Preview;
			var currentPageIndex:int = 0;
			var page:Sprite;
			
			for (var i:int = 0; i < locationModelsList.length; i++) {
				currentPageIndex = Math.floor(i / DISPLAYED_ITEMS_NUM);
				if (pagesList.length > currentPageIndex) {
					page = pagesList[currentPageIndex];
				} else {
					page = new Sprite();
					pagesList.push(page);
				}
				
				preview = new Preview(locationModelsList[i]);
				preview.x = (preview.width + SPACING_H) * (i % 2);
				preview.y = (preview.height + SPACING_V) * Math.floor(i / 2);
				
				preview.addEventListener(TouchEvent.TOUCH, onPreviewTouch);
				
				page.addChild(preview);
			}
			
			for (i = 0; i < pagesList.length; i++) {
				page = pagesList[i];
				page.x = (helper.stageArea.width * i) + MappingManager.getCentricPoint(helper.stageArea.width, pagesList[0].width);
				addChild(page);
			}
		}
		
		private function onPreviewTouch(event:TouchEvent):void {
			var touch:Touch = event.getTouch(stage);
			if (!touch) return;
			var preview:Preview = event.currentTarget as Preview;
			if (touch.phase === TouchPhase.ENDED) {
				preview.onTapEnded();
				selectedPreview = preview;
				dispatchEventWith(Event.SELECT);
			} else if (touch.phase === TouchPhase.BEGAN) {
				preview.onTapBegan();
			}
		}
		
		/**
		 * 
		 */
		
		public function get selectedPreviewName():String {
			return selectedPreview.locationName;
		}
		
	}

}