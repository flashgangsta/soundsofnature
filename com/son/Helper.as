package com.son {
	import flash.geom.Rectangle;
	
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	
	public class Helper {
		
		static private var instance:Helper;
		
		private var _stageArea:Rectangle;
		private var isInit:Boolean = false;
		
		/**
		 * 
		 */
		
		public function Helper() {
			if(!instance) {
				instance = this;
			} else {
				throw new Error("Helper is singletone. Use static funtion getInstance() for get an instance of class");
			}
		}
		
		/**
		 * 
		 */
		
		static public function getInstance():Helper {
			if(!instance) {
				instance = new Helper();
			}
			return instance;
		}
		
		/**
		 * 
		 */
		
		public function init(stageArea:Rectangle):void {
			if (isInit) {
				throw new Error("Helper was previously initialized");
			}
			isInit = true;
			_stageArea = stageArea;
			trace(_stageArea);
		}
		
		public function get stageArea():Rectangle {
			return _stageArea;
		}
		
	}
	
}