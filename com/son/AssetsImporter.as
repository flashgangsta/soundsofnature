package com.son {
	import assets.fonts.FontLight;
	import assets.sounds.*;
	import flash.text.Font;
	
	
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	
	public class AssetsImporter {
		
		static private var instance:AssetsImporter;
		
		//Fonts
		
		static public const FONT:Font = new FontLight();
		
		//Previews
		
		[Embed(source="../../assets/bitmaps/locations/previews/farm.atf", mimeType="application/octet-stream")]
		static public const FarmPreview:Class;
		[Embed(source="../../assets/bitmaps/locations/previews/forest.atf", mimeType="application/octet-stream")]
		static public const ForestPreview:Class;
		[Embed(source="../../assets/bitmaps/locations/previews/mountains.atf", mimeType="application/octet-stream")]
		static public const MountainsPreview:Class;
		[Embed(source="../../assets/bitmaps/locations/previews/sea.atf", mimeType="application/octet-stream")]
		static public const SeaPreview:Class;
		
		//FARM
		
		[Embed(source="../../assets/bitmaps/locations/farm/01.atf", mimeType="application/octet-stream")]
		static public const Farm01:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/02.atf", mimeType="application/octet-stream")]
		static public const Farm02:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/03.atf", mimeType="application/octet-stream")]
		static public const Farm03:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/04.atf", mimeType="application/octet-stream")]
		static public const Farm04:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/05.atf", mimeType="application/octet-stream")]
		static public const Farm05:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/06.atf", mimeType="application/octet-stream")]
		static public const Farm06:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/07.atf", mimeType="application/octet-stream")]
		static public const Farm07:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/08.atf", mimeType="application/octet-stream")]
		static public const Farm08:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/09.atf", mimeType="application/octet-stream")]
		static public const Farm09:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/10.atf", mimeType="application/octet-stream")]
		static public const Farm10:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/11.atf", mimeType="application/octet-stream")]
		static public const Farm11:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/12.atf", mimeType="application/octet-stream")]
		static public const Farm12:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/13.atf", mimeType="application/octet-stream")]
		static public const Farm13:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/14.atf", mimeType="application/octet-stream")]
		static public const Farm14:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/15.atf", mimeType="application/octet-stream")]
		static public const Farm15:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/16.atf", mimeType="application/octet-stream")]
		static public const Farm16:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/17.atf", mimeType="application/octet-stream")]
		static public const Farm17:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/18.atf", mimeType="application/octet-stream")]
		static public const Farm18:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/19.atf", mimeType="application/octet-stream")]
		static public const Farm19:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/20.atf", mimeType="application/octet-stream")]
		static public const Farm20:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/21.atf", mimeType="application/octet-stream")]
		static public const Farm21:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/22.atf", mimeType="application/octet-stream")]
		static public const Farm22:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/23.atf", mimeType="application/octet-stream")]
		static public const Farm23:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/24.atf", mimeType="application/octet-stream")]
		static public const Farm24:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/25.atf", mimeType="application/octet-stream")]
		static public const Farm25:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/26.atf", mimeType="application/octet-stream")]
		static public const Farm26:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/27.atf", mimeType="application/octet-stream")]
		static public const Farm27:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/28.atf", mimeType="application/octet-stream")]
		static public const Farm28:Class;
		[Embed(source="../../assets/bitmaps/locations/farm/29.atf", mimeType="application/octet-stream")]
		static public const Farm29:Class;
		
		//FOREST
		
		[Embed(source="../../assets/bitmaps/locations/forest/01.atf", mimeType="application/octet-stream")]
		static public const Forest01:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/02.atf", mimeType="application/octet-stream")]
		static public const Forest02:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/03.atf", mimeType="application/octet-stream")]
		static public const Forest03:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/04.atf", mimeType="application/octet-stream")]
		static public const Forest04:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/05.atf", mimeType="application/octet-stream")]
		static public const Forest05:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/06.atf", mimeType="application/octet-stream")]
		static public const Forest06:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/07.atf", mimeType="application/octet-stream")]
		static public const Forest07:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/08.atf", mimeType="application/octet-stream")]
		static public const Forest08:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/09.atf", mimeType="application/octet-stream")]
		static public const Forest09:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/10.atf", mimeType="application/octet-stream")]
		static public const Forest10:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/11.atf", mimeType="application/octet-stream")]
		static public const Forest11:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/12.atf", mimeType="application/octet-stream")]
		static public const Forest12:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/13.atf", mimeType="application/octet-stream")]
		static public const Forest13:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/14.atf", mimeType="application/octet-stream")]
		static public const Forest14:Class;
		[Embed(source="../../assets/bitmaps/locations/forest/15.atf", mimeType="application/octet-stream")]
		static public const Forest15:Class;
		
		//MOUNTAINS
		
		[Embed(source="../../assets/bitmaps/locations/mountains/01.atf", mimeType="application/octet-stream")]
		static public const Mountains01:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/02.atf", mimeType="application/octet-stream")]
		static public const Mountains02:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/03.atf", mimeType="application/octet-stream")]
		static public const Mountains03:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/04.atf", mimeType="application/octet-stream")]
		static public const Mountains04:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/05.atf", mimeType="application/octet-stream")]
		static public const Mountains05:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/06.atf", mimeType="application/octet-stream")]
		static public const Mountains06:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/07.atf", mimeType="application/octet-stream")]
		static public const Mountains07:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/08.atf", mimeType="application/octet-stream")]
		static public const Mountains08:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/09.atf", mimeType="application/octet-stream")]
		static public const Mountains09:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/10.atf", mimeType="application/octet-stream")]
		static public const Mountains10:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/11.atf", mimeType="application/octet-stream")]
		static public const Mountains11:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/12.atf", mimeType="application/octet-stream")]
		static public const Mountains12:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/13.atf", mimeType="application/octet-stream")]
		static public const Mountains13:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/14.atf", mimeType="application/octet-stream")]
		static public const Mountains14:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/15.atf", mimeType="application/octet-stream")]
		static public const Mountains15:Class;
		[Embed(source="../../assets/bitmaps/locations/mountains/16.atf", mimeType="application/octet-stream")]
		static public const Mountains16:Class;
		
		//SEA
		
		[Embed(source="../../assets/bitmaps/locations/sea/01.atf", mimeType="application/octet-stream")]
		static public const Sea01:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/02.atf", mimeType="application/octet-stream")]
		static public const Sea02:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/03.atf", mimeType="application/octet-stream")]
		static public const Sea03:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/04.atf", mimeType="application/octet-stream")]
		static public const Sea04:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/05.atf", mimeType="application/octet-stream")]
		static public const Sea05:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/06.atf", mimeType="application/octet-stream")]
		static public const Sea06:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/07.atf", mimeType="application/octet-stream")]
		static public const Sea07:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/08.atf", mimeType="application/octet-stream")]
		static public const Sea08:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/09.atf", mimeType="application/octet-stream")]
		static public const Sea09:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/10.atf", mimeType="application/octet-stream")]
		static public const Sea10:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/11.atf", mimeType="application/octet-stream")]
		static public const Sea11:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/12.atf", mimeType="application/octet-stream")]
		static public const Sea12:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/13.atf", mimeType="application/octet-stream")]
		static public const Sea13:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/14.atf", mimeType="application/octet-stream")]
		static public const Sea14:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/15.atf", mimeType="application/octet-stream")]
		static public const Sea15:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/16.atf", mimeType="application/octet-stream")]
		static public const Sea16:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/17.atf", mimeType="application/octet-stream")]
		static public const Sea17:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/18.atf", mimeType="application/octet-stream")]
		static public const Sea18:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/19.atf", mimeType="application/octet-stream")]
		static public const Sea19:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/20.atf", mimeType="application/octet-stream")]
		static public const Sea20:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/21.atf", mimeType="application/octet-stream")]
		static public const Sea21:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/22.atf", mimeType="application/octet-stream")]
		static public const Sea22:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/23.atf", mimeType="application/octet-stream")]
		static public const Sea23:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/24.atf", mimeType="application/octet-stream")]
		static public const Sea24:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/25.atf", mimeType="application/octet-stream")]
		static public const Sea25:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/26.atf", mimeType="application/octet-stream")]
		static public const Sea26:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/27.atf", mimeType="application/octet-stream")]
		static public const Sea27:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/28.atf", mimeType="application/octet-stream")]
		static public const Sea28:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/29.atf", mimeType="application/octet-stream")]
		static public const Sea29:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/30.atf", mimeType="application/octet-stream")]
		static public const Sea30:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/31.atf", mimeType="application/octet-stream")]
		static public const Sea31:Class;
		[Embed(source="../../assets/bitmaps/locations/sea/32.atf", mimeType="application/octet-stream")]
		static public const Sea32:Class;
		
		/**
		 * 
		 */
		
		public function AssetsImporter() {
			Birds;
			Crickets;
			Cuckoo;
			Frogs;
			River;
			Storm;
			Woodpecker;
			
			if(!instance) {
				instance = this;
				init();
			} else {
				throw new Error("AssetsImporter is singletone. Use static funtion getInstance() for get an instance of class");
			}
		}
		
		/**
		 * 
		 */
		
		static public function getInstance():AssetsImporter {
			if(!instance) {
				instance = new AssetsImporter();
			}
			return instance;
		}
		
		/**
		 * 
		 */
		
		private function init():void {
			
		}
		
	}
	
}