package com.son {
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	
	public class ConfigModel {
		
		static public const FOREST:String = "Forest";
		static public const SEA:String = "Sea";
		static public const FARM:String = "Farm";
		static public const MOUNTAINS:String = "Mountains";
		
		static private var instance:ConfigModel;
		
		private var locationsNum:Number;
		private var locationModelsByName:Dictionary = new Dictionary();
		private var _locationModelsList:Vector.<LocationModel> = new Vector.<LocationModel>();
		
		/**
		 * 
		 */
		
		public function ConfigModel() {
			if(!instance) {
				instance = this;
			} else {
				throw new Error("ConfigModel is singletone. Use static funtion getInstance() for get an instance of class");
			}
		}
		
		/**
		 * 
		 */
		
		static public function getInstance():ConfigModel {
			if(!instance) {
				instance = new ConfigModel();
			}
			return instance;
		}
		
		/**
		 * 
		 */
		
		public function init(configXML:XML):void {
			const locationsList:XMLList = configXML.locations.location;
			var locationModel:LocationModel;
			locationsNum = locationsList.length();
			
			for (var i:int = 0; i < locationsNum; i++) {
				locationModel = new LocationModel(locationsList[i]);
				_locationModelsList.push(locationModel);
				locationModelsByName[locationModel.name.toLowerCase()] = locationModel;
			}
		}
		
		/**
		 * 
		 */
		
		public function getLocationModelByName(locationName:String):LocationModel {
			return locationModelsByName[locationName.toLowerCase()];
		}
		
		/**
		 * 
		 */
		
		public function get locationModelsList():Vector.<LocationModel> {
			return new Vector.<LocationModel>().concat(_locationModelsList);
		}
		
	}
	
}