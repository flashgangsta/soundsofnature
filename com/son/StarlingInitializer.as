package com.son {
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import starling.core.Starling;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	
	[SWF(width="1280", height="720", frameRate="60", backgroundColor="#FFFFFF")]
	
	public class StarlingInitializer extends Sprite {
		
		private var _starling:Starling;
		
		/**
		 * 
		 */
		
		public function StarlingInitializer() {
			super();
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(flash.events.Event.RESIZE, onStageRisized);
			
			Starling.handleLostContext = true;
			
			_starling = new Starling(Main, stage);
			_starling.addEventListener(starling.events.Event.ROOT_CREATED, onRootCreated);
			_starling.start();
		}
		
		
		/**
		 * 
		 * @param	event
		 */
		
		private function onRootCreated(event:starling.events.Event):void {
			onStageRisized();
			_starling.showStatsAt("left", "bottom", 2);
		}
		
		/**
		 * 
		 * @param	event
		 */
		
		private function onStageRisized(event:flash.events.Event = null):void {
			if (_starling) {
				_starling.viewPort.x = 0;
				_starling.viewPort.y = 0;
				_starling.viewPort.width = stage.stageWidth;
				_starling.viewPort.height = stage.stageHeight;
				_starling.stage.stageWidth = stage.stageWidth;
				_starling.stage.stageHeight = stage.stageHeight;
			}
		}
		
	}
	
}