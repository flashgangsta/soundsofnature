package com.son {
	import com.flashgangsta.managers.MappingManager;
	import com.flashgangsta.utils.StringUtil;
	import flash.geom.Point;
	import flash.utils.ByteArray;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.filters.BlurFilter;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	public class Preview extends Sprite {
		private var locationModel:LocationModel;
		private var label:TextField;
		private var image:Image;
		private var center:Point;
		
		public function Preview(locationModel:LocationModel) {
			this.locationModel = locationModel;
			var textureClassName:String;
			var textureClass:Class;
			var labelFilter:BlurFilter = BlurFilter.createDropShadow(1, .785, 0, 1, 0);
			
			textureClassName = StringUtil.getStringWithCapitalLetter(locationModel.name) + "Preview";
			textureClass = AssetsImporter[textureClassName] as Class;
			image = new Image(Texture.fromAtfData(new textureClass() as ByteArray));
			
			center = new Point(int(image.width / 2), int(image.height / 2));
			
			label = new TextField(0, 0, StringUtil.getStringWithCapitalLetter(locationModel.name), AssetsImporter.FONT.fontName, 48, 0xFFFFFF);
			label.filter = labelFilter;
			label.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
			label.touchable = false;
			
			MappingManager.alignToCenterByX(label, image.bounds);
			MappingManager.alignToBottom(label, image.bounds, -5);
			
			addChild(image);
			addChild(label);
		}
		
		/**
		 * 
		 */
		
		public function onTapBegan():void {
			image.scaleX = image.scaleY = .98;
			image.x = center.x - (image.width / 2);
			image.y = center.y - (image.height / 2);
		}
		
		/**
		 * 
		 */
		
		public function onTapEnded():void {
			image.scaleX = image.scaleY = 1;
			image.x = image.y = 0;
		}
		
		/**
		 * 
		 */
		
		public function get locationName():String {
			return locationModel.name;
		}
		
	}

}