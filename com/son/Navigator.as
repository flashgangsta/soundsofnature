package com.son {
	import feathers.controls.ScreenNavigator;
	import feathers.controls.ScreenNavigatorItem;
	import feathers.motion.transitions.ScreenSlidingStackTransitionManager;
	
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	
	public class Navigator extends ScreenNavigator {
		static public const MAIN_MENU:String = "MainMenu";
		static public const LOCATION:String = "Location";
		
		static private var instance:Navigator;
		
		private var mainMenu:MainMenuScreen = new MainMenuScreen();
		private var location:LocationScreen = new LocationScreen();
		private var transitionManager:ScreenSlidingStackTransitionManager;
		
		/**
		 * 
		 */
		
		public function Navigator() {
			if(!instance) {
				instance = this;
				init();
			} else {
				throw new Error("Navigator is singletone. Use static funtion getInstance() for get an instance of class");
			}
		}
		
		/**
		 * 
		 */
		
		static public function getInstance():Navigator {
			if(!instance) {
				instance = new Navigator();
			}
			return instance;
		}
		
		/**
		 * 
		 */
		
		private function init():void {
			super.addScreen(Navigator.MAIN_MENU, new ScreenNavigatorItem(mainMenu));
			super.addScreen(Navigator.LOCATION, new ScreenNavigatorItem(location));
		}
		
	}
	
}