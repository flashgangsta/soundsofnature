package com.son {
	import feathers.themes.MetalWorksMobileTheme;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	
	public class Main extends Sprite {
		[Embed(source="../../assets/config.xml", mimeType="application/octet-stream")]
		private const CONFIG:Class;
		private var configModel:ConfigModel = ConfigModel.getInstance();
		
		private var navigator:Navigator = Navigator.getInstance();
		
		private var locationScreen:LocationScreen;
		private var mainMenuScreen:MainMenuScreen;
		
		public function Main() {
			new AssetsImporter();
			new MetalWorksMobileTheme();
			
			Helper.getInstance().init(Starling.current.viewPort);
			
			configModel.init(new XML(new CONFIG().toString()));
			
			mainMenuScreen = navigator.showScreen(Navigator.MAIN_MENU) as MainMenuScreen;
			mainMenuScreen.addEventListener(Event.SELECT, onLocationSelected);
			
			addChild(navigator);
		}
		
		/**
		 * 
		 * @param	event
		 */
		
		private function onLocationSelected(event:Event):void {
			trace("onLocationSelected:", event.data);
			
			navigator.removeScreen(Navigator.MAIN_MENU);
			locationScreen = navigator.showScreen(Navigator.LOCATION) as LocationScreen;
			locationScreen.initLocation(configModel.getLocationModelByName(event.data as String));
		}
		
	}

}