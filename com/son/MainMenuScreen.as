package com.son {
	import com.flashgangsta.managers.MappingManager;
	import feathers.controls.PageIndicator;
	import feathers.controls.Screen;
	import feathers.events.FeathersEventType;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	public class MainMenuScreen extends AppScreen {
		
		private var pageIndicator:PageIndicator = new PageIndicator();
		private var locationModelsList:Vector.<LocationModel>
		private var previewsContainer:PreviewsContainer = new PreviewsContainer();
		
		public function MainMenuScreen() {
			super();
			
		}
		
		override protected function initialize():void {
			super.initialize();
			trace("Main menu init");
			
			locationModelsList = ConfigModel.getInstance().locationModelsList;
			
			trace(locationModelsList);
			
			pageIndicator.pageCount = Math.ceil(locationModelsList.length / PreviewsContainer.DISPLAYED_ITEMS_NUM);
			pageIndicator.validate();
			pageIndicator.addEventListener(FeathersEventType.CREATION_COMPLETE, onPageIndicatorCreated);
			
			previewsContainer.addEventListener(Event.SELECT, onLocationSelect);
			previewsContainer.init(locationModelsList);
			previewsContainer.y = 70;
			
			addChild(previewsContainer);
			addChild(pageIndicator);
		}
		
		/**
		 * 
		 * @param	event
		 */
		
		private function onLocationSelect(event:Event):void {
			dispatchEventWith(Event.SELECT, false, previewsContainer.selectedPreviewName);
		}
		
		/**
		 * 
		 * @param	event
		 */
		
		private function onPageIndicatorCreated(event:Event):void {
			pageIndicator.removeEventListener(FeathersEventType.CREATION_COMPLETE, onPageIndicatorCreated);
			MappingManager.alignToCenterByX(pageIndicator, Helper.getInstance().stageArea);
			pageIndicator.y = previewsContainer.bounds.bottom + MappingManager.getCentricPoint(helper.stageArea.height - previewsContainer.bounds.bottom, pageIndicator.height);
			
		}
		
	}

}