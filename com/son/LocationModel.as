package com.son {
	import com.flashgangsta.utils.StringUtil;
	import feathers.events.CollectionEventType;
	import flash.utils.getDefinitionByName;
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	public class LocationModel {
		
		private var _imagesNum:int;
		private var _name:String;
		private var _price:Number;
		private var soundNamesList:Vector.<String> = new Vector.<String>();
		private var soundClassesList:Vector.<Class> = new Vector.<Class>();
		
		/**
		 * 
		 * @param	locationXML
		 */
		
		public function LocationModel(locationXML:XML) {
			const soundsList:XMLList = locationXML.sounds.sound;
			const soundsNum:int = soundsList.length();
			var soundName:String;
			var soundClassName:String;
			
			_name = locationXML.@name;
			_imagesNum = locationXML.@imagesNum;
			_price = locationXML.@price;
			
			for (var i:int = 0; i < soundsNum; i++) {
				soundName = soundsList[i].@name;
				soundClassName = StringUtil.getStringWithCapitalLetter(soundName);
				soundNamesList.push(soundName);
				soundClassesList.push(getDefinitionByName("assets.sounds." + soundClassName));
			}
			
		}
		
		public function get imagesNum():int {
			return _imagesNum;
		}
		
		public function get name():String {
			return _name;
		}
		
		public function get price():Number {
			return _price;
		}
		
	}

}