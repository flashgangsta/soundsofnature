package com.son {
	import caurina.transitions.Tweener;
	import com.flashgangsta.managers.MappingManager;
	import com.flashgangsta.utils.ArrayMixer;
	import com.flashgangsta.utils.executeAfrerFrame;
	import com.flashgangsta.utils.NumUtil;
	import feathers.controls.ProgressBar;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.system.System;
	import flash.utils.ByteArray;
	import flash.utils.getTimer;
	import flash.utils.Timer;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	public class LocationImagesViewer extends Sprite {
		
		private const TEXTURE_MAX_SIZE:Number = 2048;
		private const MAX_SCALE:Number = 1.2;
		private const EFFECT_TIME:int = 15;
		private const DELAY_BETWEEN_IMAGES:int = 10;
		private const FADE_OUT_IMAGE_TIME:int = 4;
		private const timer:Timer = new Timer(DELAY_BETWEEN_IMAGES * 1000, 1);
		
		private var progressBar:ProgressBar = new ProgressBar();
		private var imagesNum:int;
		private var imagesClassPrefix:String;
		private var texturesList:Vector.<Texture> = new Vector.<Texture>();
		private var helper:Helper = Helper.getInstance();
		private var currentImageHolder:Sprite;
		private var lastImageHolder:Sprite;
		private var currentImageIndex:int = 0;
		private var indexesQueue:Vector.<int> = new Vector.<int>();
		private var texturesScale:Number = 1;
		private var imagesSmoothing:String = TextureSmoothing.NONE;
		private var texturesUploaded:int = 0;
		
		
		/**
		 * 
		 */
		
		public function LocationImagesViewer() {
			super();
		}
		
		/**
		 * 
		 * @param	imagesNum
		 * @param	imagesClassPrefix
		 */
		
		public function init(imagesNum:int, imagesClassPrefix:String):void {
			progressBar.value = 0;
			progressBar.width = 200;
			progressBar.height = 15;
			
			MappingManager.alignToCenter(progressBar, helper.stageArea);
			
			timer.addEventListener(TimerEvent.TIMER, onTimer);
			
			if (TextureSmoothing.isValid(TextureSmoothing.TRILINEAR)) {
				imagesSmoothing = TextureSmoothing.TRILINEAR;
			} else if (TextureSmoothing.isValid(TextureSmoothing.BILINEAR)) {
				imagesSmoothing = TextureSmoothing.BILINEAR;
			}
			
			trace("smoothing:", imagesSmoothing);
			
			this.imagesClassPrefix = imagesClassPrefix;
			this.imagesNum = imagesNum;
			progressBar.value = 0;
			addChild(progressBar);
			uploadTextures();
		}
		
		/**
		 * 
		 */
		
		public function startAnimation():void {
			removeChild(progressBar);
			indexesQueue = Vector.<int>(ArrayMixer.getMixedNumbersArray(0, imagesNum - 1));
			currentImageIndex = 0;
			addImage();
		}
		
		/**
		 * 
		 */
		
		public function stopAnimation():void {
			trace("stopAnimation");
		}
		
		/**
		 * 
		 */
		
		public function clear():void {
			var texture:Texture;
			var image:Image;
			
			stopAnimation();
			removeChildren();
			
			while (numChildren) {
				image = getChildAt(numChildren - 1) as Image;
				image.dispose();
			}
			
			while (texturesList.length) {
				texture = texturesList.pop() as Texture;
				texture.dispose();
			}
			
			System.gc();
			
			trace("cleared");
		}
		
		/**
		 * 
		 */
		
		private function uploadTextures():void {
			trace("uploadTextures");
			var textureClassName:String;
			var texture:Texture;
			
			for (var i:int = 0; i < imagesNum; i++) {
				textureClassName = imagesClassPrefix + NumUtil.toTwoDigit(i + 1);
				texture = Texture.fromAtfData(new AssetsImporter[textureClassName]() as ByteArray, 1, true, onTextureUploaded); 
				texturesList.push(texture);
			}
			
		}
		
		/**
		 * 
		 */
		
		private function onTextureUploaded():void {
			texturesUploaded++;
			progressBar.value = texturesUploaded / imagesNum;
			
			if (texturesUploaded === imagesNum) {
				trace("All location textures is init");
				trace(getTimer());
				executeAfrerFrame(Starling.current.nativeStage, startAnimation);
			}
		}
		
		/**
		 * 
		 */
		
		private function addImage():void {
			var textureIndex:int = indexesQueue[currentImageIndex];
			var currentImage:Image = new Image(texturesList[textureIndex]);
			trace("show image:", currentImageIndex, "(index:", textureIndex + ")");
			currentImage.smoothing = imagesSmoothing;
			MappingManager.setScaleFillArea(currentImage, helper.stageArea);
			currentImageHolder = new Sprite();
			currentImageHolder.addChild(currentImage);
			addEffects();
			addChildAt(currentImageHolder, 0);
			timer.reset();
			timer.start();
		}
		
		/**
		 * 
		 */
		
		private function addEffects():void {
			var motionParams:Object;
			
			if (Math.random() < .55) {
				trace("zoomIn");
				motionParams = {
					scaleX: MAX_SCALE,
					scaleY: MAX_SCALE,
					time: EFFECT_TIME,
					transition: "linear",
					onUpdate: function():void {
						MappingManager.alignToCenter(this, helper.stageArea, false);
					}
				};
			} else {
				trace("zoomOut");
				currentImageHolder.scaleX = currentImageHolder.scaleY = MAX_SCALE;
				motionParams = {
					scaleX: 1,
					scaleY: 1,
					time: EFFECT_TIME,
					transition: "linear",
					onUpdate: function():void {
						MappingManager.alignToCenter(this, helper.stageArea, false);
					}
				};
			}
			
			Tweener.addTween(currentImageHolder, motionParams);
		}
		
		/**
		 * 
		 * @param	event
		 */
		
		private function onTimer(event:TimerEvent):void {
			trace("onTimer");
			lastImageHolder = currentImageHolder;
			Tweener.addTween(currentImageHolder, {
				alpha: 0,
				time: FADE_OUT_IMAGE_TIME,
				transition: "easeInCubic",
				onComplete: disposeLastImageHolder
			});
			currentImageIndex = (currentImageIndex + 1) % imagesNum;
			addImage();
		}
		
		/**
		 * 
		 */
		
		private function disposeLastImageHolder():void {
			var image:Image = lastImageHolder.getChildAt(0) as Image;
			removeChild(lastImageHolder);
			lastImageHolder.removeChild(image);
			image.dispose();
			lastImageHolder.dispose();
			
			image = null;
			lastImageHolder = null;
			
			System.gc();
		}
		
	}

}