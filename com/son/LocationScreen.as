package com.son {
	import feathers.controls.Button;
	import feathers.controls.Header;
	import feathers.controls.Screen;
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import starling.display.DisplayObject;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Sergey Krivtsov (flashgangsta@gmail.com)
	 */
	public class LocationScreen extends AppScreen {
		
		private var imagesViewer:LocationImagesViewer = new LocationImagesViewer();
		private var header:Header = new Header();
		private var backButton:Button = new Button();
		private var locationModel:LocationModel;
		
		public function LocationScreen() {
			super();
		}
		
		override protected function initialize():void {
			trace("LocationScreenInitialize");
			super.initialize();
			
			backButton.label = "Back";
			header.width = stage.stageWidth;
			header.height = 30;
			header.paddingLeft = 100;
			header.leftItems = new <DisplayObject>[backButton];
			header.y = -header.height;
			backButton.nameList.add(Button.ALTERNATE_NAME_BACK_BUTTON);
			backButton.addEventListener(Event.TRIGGERED, onBackButtonClicked);
			
			addChild(imagesViewer);
			addChild(header);
		}
		
		/**
		 * 
		 * @param	event
		 */
		
		private function onBackButtonClicked(event:Event):void {
			trace("back");
		}
		
		/**
		 * 
		 */
		
		override protected function draw():void {
			super.draw();
		}
		
		/**
		 * 
		 * @param	event
		 */
		
		override protected function screen_removedFromStageHandler(event:Event):void {
			super.screen_removedFromStageHandler(event);
			imagesViewer.clear();
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
		}
		
		/**
		 * 
		 * @param	locationModel
		 */
		
		public function initLocation(locationModel:LocationModel):void {
			this.locationModel = locationModel;
			imagesViewer.init(locationModel.imagesNum, locationModel.name);
			header.title = locationModel.name;
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
		}
	}

}